export interface IProgram {
    id?: number;
    name?: string;
    premium?: boolean;
}

export class Program {

    public id: number;
    public name: string;
    public premium?: boolean;

    constructor(iProgram: IProgram) {
        if (iProgram) {
            this.id = iProgram.id;
            this.name = iProgram.name;
            this.premium = iProgram.premium;
        }
    }

}
