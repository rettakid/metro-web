import { IImageFile, ImageFile } from '../file/image-file.model';

export interface IUser {
    id?: number;
    email?: string;
    name?: string;
    surname?: string;
    dateOfBirth?: string;
    image?: IImageFile;
}

export class User {

    public id: number;
    public email: string;
    public name: string;
    public surname: string;
    public dateOfBirth: string;
    public image: IImageFile;

    constructor(iUser: IUser) {
        if (iUser) {
            this.id = iUser.id;
            this.email = iUser.email;
            this.name = iUser.name;
            this.surname = iUser.surname;
            this.dateOfBirth = iUser.dateOfBirth;
            this.image = iUser.image ? new ImageFile(iUser.image) : null;
        }
    }

}
