import { IProgram, Program } from '../program/program.model';
import { IUser, User } from './user.model';

export interface IUserProgram {
    user?: IUser;
    program?: IProgram;
}

export class UserProgram {

    public user: User;
    public program: Program;

    constructor(iUserProgram: IUserProgram) {
        if (iUserProgram) {
            this.user = new User(iUserProgram.user);
            this.program = new Program(iUserProgram.program);
        }
    }

}
