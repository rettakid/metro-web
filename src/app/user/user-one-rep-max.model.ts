import { IUser, User } from './user.model';
import { IExerciseType, ExerciseType } from '../exercise/exercise-type/exercise-type.model';

export interface IUserOneRepMax {
    user?: IUser;
    exerciseType?: IExerciseType;
    weight?: number;
    reps?: number;
    oneRepMax?: number;
}

export class UserOneRepMax {

    public user: IUser;
    public exerciseType: ExerciseType;
    public weight: number;
    public reps: number;
    public oneRepMax: number;

    constructor(iUserOneRepMax: IUserOneRepMax) {
        if (iUserOneRepMax) {
            this.user = new User(iUserOneRepMax.user);
            this.exerciseType = new ExerciseType(iUserOneRepMax.exerciseType);
            this.weight = iUserOneRepMax.weight;
            this.reps = iUserOneRepMax.reps;
            this.oneRepMax = iUserOneRepMax.oneRepMax;
        }
    }

}
