import { NgModule } from '@angular/core';
import { WorkoutCalendarComponent } from './workout-calendar/workout-calendar.component';
import { WorkoutDayComponent } from './workout-day/workout-day.component';
import { SharedModule } from '../shared/shared.module';
import { WorkoutDayDetailsComponent } from './workout-day/workout-day-details/workout-day-details.component';
import { WorkoutTrackerComponent } from './workout-tracker/workout-tracker.component';
import { WorkoutDayExerciseComponent } from './workout-day/workout-day-exercise/workout-day-exercise.component';

@NgModule({
  imports: [
    SharedModule
  ],
  exports: [
    WorkoutCalendarComponent,
    WorkoutTrackerComponent
  ],
  declarations: [
    WorkoutCalendarComponent, WorkoutDayComponent, WorkoutDayDetailsComponent, WorkoutTrackerComponent, WorkoutDayExerciseComponent
  ]
})
export class WorkoutModule { }
