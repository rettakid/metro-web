import { Component, OnInit } from '@angular/core';
import { WorkoutDay } from './workout-day.model';
import { Workout } from '../workout.model';
import { Exercise } from '../../exercise/exercise/exercise.model';
import { ExerciseTypeService } from '../../exercise/exercise-type/exercise-type.service';
import { Router, ActivatedRoute } from '@angular/router';
import { WorkoutExercise } from '../../exercise/workout-exercise/workout-exercise.model';
import * as moment from 'moment';
import { ExerciseType } from '../../exercise/exercise-type/exercise-type.model';
import { ModalService } from '../../shared/modal/modal.service';
import { ExerciseSet } from '../../exercise/exercise-set/exercise-set.model';
import { StorageService } from '../../shared/storage/storage.service';
import { StorageEnum } from '../../shared/storage/storage.enum';
import { WorkoutService } from '../workout.service';
import { Program } from '../../program/program.model';

@Component({
  selector: 'app-workout-day',
  templateUrl: './workout-day.component.html',
  styleUrls: ['./workout-day.component.scss']
})
export class WorkoutDayComponent implements OnInit {

  public workoutDay: WorkoutDay;
  public workoutId: number;
  public exerciseTypes: ExerciseType[];
  public activeWorkoutExerciseIndex: number;
  public activeExerciseIndex: number;
  public activeWorkoutExercise: WorkoutExercise;
  public exerciseFormComplete = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private exerciseTypeService: ExerciseTypeService,
    private storageService: StorageService,
    private workoutService: WorkoutService
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(
      params => this.getWorkouts(params['date'])
    );
    this.exerciseTypeService.getAllExerciseTypes().subscribe(
      exerciseTypes => this.exerciseTypes = exerciseTypes
    );

    // remove me
    this.storageService.set(StorageEnum.USER, { id: 1 });
  }

  // gets and setters

  get workout(): Workout {
    const workouts: Workout[] = [];
    return workouts
      .concat(this.workoutDay.completedWorkouts, this.workoutDay.expectedWorkouts)
      .find(workout => workout.id === this.workoutId);
  }

  set workout(workout: Workout) {
    const workoutIndex: number = workout.workoutDate
      ? this.workoutDay.completedWorkouts.findIndex(workoutItem => workoutItem.id === this.workoutId)
      : this.workoutDay.expectedWorkouts.findIndex(workoutItem => workoutItem.id === this.workoutId);
    workout.workoutDate
      ? this.workoutDay.completedWorkouts[workoutIndex] = workout
      : this.workoutDay.expectedWorkouts[workoutIndex] = workout;
    this.workoutId = workout.id;
  }

  get incompleteWorkout(): boolean {
    return this.workoutDay && !!this.workoutDay.expectedWorkouts.find(
      workout => !workout.id
    );
  }

  // events

  public onSelectWorkout(workout: Workout) {
    this.workoutId = workout ? workout.id : undefined;
  }

  public onAddWorkout() {
    const time = new Date().getTime();
    const workout = new Workout({
      name: moment(time).format('DD-MMM hh:mm:ss'),
      sequenceNumber: moment(this.workoutDay.date).diff(moment('2000-01-01'), 'days'),
      user: this.storageService.get(StorageEnum.USER),
      workoutExercises: []
    });
    this.workoutDay.expectedWorkouts.push(workout);
    this.workoutId = workout.id;
  }

  public onSaveWorkout() {
    this.workoutService.saveWorkout(this.workout).subscribe(
      workout => this.workout = workout
    );
  }

  public onAddExercise() {
    this.activeWorkoutExercise = new WorkoutExercise({
      restPeriod: 0,
      exercises: [
        new Exercise({
          sequenceNumber: this.activeWorkoutExercise
            ? this.getMaxValue(this.activeWorkoutExercise.exercises.map(exercise => exercise.sequenceNumber)) + 1
            : 1,
          restPeriod: 0,
          sets: [{
            sequenceNumber: 1,
            toFailure: false,
            restPeriod: 60
          }]
        })
      ]
    });
  }

  public onSelectWorkoutExercise(workoutExercise: WorkoutExercise) {
    this.activeWorkoutExercise = workoutExercise;
  }

  public onSaveActiveExercise(workoutExercise: WorkoutExercise) {
    if (!this.workout.workoutExercises.includes(workoutExercise)) {
      this.workout.workoutExercises.push(workoutExercise);
    }
  }

  public onCloseExercise() {
    this.activeWorkoutExercise = null;
  }

  // public

  public getDate(date: string) {
    return moment(date).format('Do MMMM YYYY');
  }

  public getSimplifiedName(name: string) {
    return name ? name.split(' ').map(word => word[0]).join('') : 'ND';
  }

  // private

  private getMaxValue(array: number[]) {
    return array.length && array.length > 0 ? Math.max.apply(Math, array) : 0;
  }

  private getWorkouts(date: string) {
    this.workoutService.getWorkoutDays(date, date).subscribe(
      workoutDays => this.workoutDay = workoutDays[0]
    );
    this.workoutDay = new WorkoutDay({ date: date });
  }

}
