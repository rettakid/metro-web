import { Workout, IWorkout } from '../workout.model';

export interface IWorkoutDay {
    date?: string;
    completedWorkouts?: IWorkout[];
    expectedWorkouts?: IWorkout[];
}

export class WorkoutDay {

    public date: string;
    public completedWorkouts: Workout[];
    public expectedWorkouts: Workout[];

    constructor(iWorkoutDay: IWorkoutDay) {
        if (iWorkoutDay) {
            this.date = iWorkoutDay.date;
            this.completedWorkouts = iWorkoutDay.completedWorkouts
                ? iWorkoutDay.completedWorkouts.map(workout => new Workout(workout)) : [];
            this.expectedWorkouts = iWorkoutDay.expectedWorkouts
                ? iWorkoutDay.expectedWorkouts.map(workout => new Workout(workout)) : [];
        }
    }

}
