import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Workout } from '../../workout.model';
import { Program } from '../../../program/program.model';
import { WorkoutService } from '../../workout.service';
import { ExerciseSet } from '../../../exercise/exercise-set/exercise-set.model';
import { ExerciseType } from '../../../exercise/exercise-type/exercise-type.model';

@Component({
    selector: 'app-workout-day-details',
    templateUrl: './workout-day-details.component.html',
    styleUrls: ['./workout-day-details.component.scss']
})
export class WorkoutDayDetailsComponent implements OnInit, OnChanges {

    @Input() workout: Workout;
    @Input() changed = false;
    @Output() select: EventEmitter<Workout> = new EventEmitter<Workout>();
    public defaultProgram: Program = new Program({ name: 'New Day' });

    constructor(
        private workoutService: WorkoutService
    ) { }

    // life cycle hooks

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.workout) {
            this.changed = true;
        }
    }

    // getters and setters

    get programName(): string {
        return this.workout.program ? this.workout.program.name : this.defaultProgram.name;
    }

    get totalTime(): string {
        let time = 0;
        if (this.workout.workoutExercises && this.workout.workoutExercises.length > 0) {
            time = this.workout.workoutExercises
                .map(workoutExercise => workoutExercise.exercises
                    .map(exercise => exercise.sets
                        .map(set => this.calculateSetTime(set, exercise.type))
                        .reduce((a, b) => this.add(a, b)) + (exercise.restPeriod || 0)
                    )
                    .reduce((a, b) => this.add(a, b)) + (workoutExercise.restPeriod || 0)
                )
                .reduce((a, b) => this.add(a, b));
        }
        return time + 's';
    }

    public add(...numbers): number {
        return numbers.reduce((a, b) => a + b);
    }

    // events

    public onWorkoutSelected() {
        this.select.emit(this.workout);
    }

    public onSaveWorkout() {
        this.changed = false;
        this.workoutService.saveWorkout(this.workout).subscribe(
            workout => this.workout
        );
    }

    // public

    public getSimplifiedName(name: string) {
        return name.split(' ').map(word => word[0]).join('');
    }

    // private

    public calculateSetTime(set: ExerciseSet, type: ExerciseType) {
        return set.restPeriod + (set.reps *
            (type.concentricPeriod + type.concentricHoldPeriod + type.eccentricPeriod + type.eccentricHoldPeriod)
        );
    }

}
