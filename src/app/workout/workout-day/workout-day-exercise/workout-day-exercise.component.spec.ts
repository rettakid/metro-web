import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkoutDayExerciseComponent } from './workout-day-exercise.component';

describe('WorkoutDayExerciseComponent', () => {
  let component: WorkoutDayExerciseComponent;
  let fixture: ComponentFixture<WorkoutDayExerciseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkoutDayExerciseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkoutDayExerciseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
