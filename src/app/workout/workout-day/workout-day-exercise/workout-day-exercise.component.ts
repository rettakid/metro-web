import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Exercise } from '../../../exercise/exercise/exercise.model';
import { ExerciseSet } from '../../../exercise/exercise-set/exercise-set.model';
import { ExerciseType } from '../../../exercise/exercise-type/exercise-type.model';
import { WorkoutExercise } from '../../../exercise/workout-exercise/workout-exercise.model';

@Component({
  selector: 'app-workout-day-exercise',
  templateUrl: './workout-day-exercise.component.html',
  styleUrls: ['./workout-day-exercise.component.scss']
})
export class WorkoutDayExerciseComponent implements OnInit {

  @Input() workoutExercise: WorkoutExercise;
  @Input() exerciseTypes: ExerciseType[];
  @Output() saveWorkoutExercise: EventEmitter<WorkoutExercise> = new EventEmitter<WorkoutExercise>();
  @Output() close: EventEmitter<void> = new EventEmitter();

  private exerciseIndex = 0;

  constructor() { }

  ngOnInit() {
  }

  // getters and setters

  get exercise() {
    return this.workoutExercise.exercises[this.exerciseIndex];
  }

  set exercise(exercise: Exercise) {
    this.workoutExercise[this.exerciseIndex] = exercise;
  }

  // events

  public onAddSet() {
    const lastSet = this.exercise.sets[this.exercise.sets.length - 1];
    this.exercise.sets.push(new ExerciseSet({
      sequenceNumber: this.getMaxValue(this.exercise.sets.map(set => set.sequenceNumber)) + 1,
      toFailure: false,
      reps: lastSet.reps,
      weight: lastSet.weight,
      restPeriod: lastSet.restPeriod
    }));
  }

  public onAddSuperSet() {
    const newExercise: Exercise = new Exercise(JSON.parse(JSON.stringify(this.exercise)));
    newExercise.sequenceNumber = this.getMaxValue(this.workoutExercise.exercises.map(exercise => exercise.sequenceNumber)) + 1;
    this.exercise.sets.forEach(set => set.restPeriod = 5);
    this.workoutExercise.exercises.push(newExercise);
    this.exerciseIndex++;
  }

  public onSaveExercise() {
    this.saveWorkoutExercise.emit(new WorkoutExercise(JSON.parse(JSON.stringify(this.workoutExercise))));
    this.onClose();
  }

  public onClose() {
    this.close.emit();
  }

  public onSelectExercise(exercise: Exercise) {
    this.exerciseIndex = this.workoutExercise.exercises.findIndex(exerciseItem => exerciseItem === exercise);
  }

  // private

  private getMaxValue(array: number[]) {
    return array.length && array.length > 0 ? Math.max.apply(Math, array) : 0;
  }

}
