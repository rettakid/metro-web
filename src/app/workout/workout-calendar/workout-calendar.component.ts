import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { WorkoutService } from '../workout.service';
import { DateUtilz } from '../../shared/utilz/date.utilz';
import { WorkoutDay } from '../workout-day/workout-day.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-workout-calendar',
  templateUrl: './workout-calendar.component.html',
  styleUrls: ['./workout-calendar.component.scss']
})
export class WorkoutCalendarComponent implements OnInit {

  public weeks: WorkoutDay[][] = [[], [], [], [], [], []];
  public activeMonth = moment().startOf('month').toDate().getTime();
  public today = moment().startOf('day').toDate().getTime();
  public weekDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thurday', 'Friday', 'Saturday'];

  constructor(
    private workoutService: WorkoutService,
    private router: Router
  ) { }



  ngOnInit() {
    this.getWorkingDays();
  }

  // events

  public onSetActiveDay(day: WorkoutDay) {
    this.router.navigateByUrl(`workout/calendar/${day.date}`);
  }

  public onNextMonthClick() {
    this.activeMonth = moment(this.activeMonth).add(1, 'month').toDate().getTime();
    this.getWorkingDays();
  }

  public onPrevMonthClick() {
    this.activeMonth = moment(this.activeMonth).subtract(1, 'month').toDate().getTime();
    this.getWorkingDays();
  }

  // public

  public getDate(date: string): string {
    return date ? String(moment(date, DateUtilz.DATE_FORMAT).get('date')) : '';
  }

  public getMonth(date: string): string {
    return moment(date).format('MMMM');
  }

  public isToday(date: string): boolean {
    return moment(date).toDate().getTime() === this.today;
  }

  // private

  private getWorkingDays() {
    this.workoutService.getWorkoutDays(
      moment(this.activeMonth).startOf('month').format(DateUtilz.DATE_FORMAT)
      , moment(this.activeMonth).endOf('month').format(DateUtilz.DATE_FORMAT)
    ).subscribe(
      workoutDays => this.setWorkoutWeeks(workoutDays)
    );
  }

  private setWorkoutWeeks(workoutDays: WorkoutDay[]) {
    this.weeks = [[], [], [], [], [], []];
    const firstWeekday = moment(workoutDays[0].date).get('weekday');
    const lastWeekday = 6 - moment(workoutDays[workoutDays.length - 1].date).get('weekday');
    const daysOfLastMonth = new Array(firstWeekday).fill(null);
    const daysOfNextMonth = new Array(lastWeekday).fill(null);
    workoutDays.unshift(...daysOfLastMonth);
    workoutDays.push(...daysOfNextMonth);
    workoutDays.forEach(
      (workoutDay, index) => this.weeks[parseInt(String(index / 7), 0)][index % 7] = workoutDay
    );
  }

}
