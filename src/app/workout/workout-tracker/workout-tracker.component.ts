import { Component, OnInit, Input } from '@angular/core';
import { ExerciseActivity } from './exercise-activity.model';
import { Workout } from '../workout.model';
import { WorkoutExercise } from '../../exercise/workout-exercise/workout-exercise.model';
import { WorkoutService } from '../workout.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-workout-tracker',
  templateUrl: './workout-tracker.component.html',
  styleUrls: ['./workout-tracker.component.scss']
})
export class WorkoutTrackerComponent implements OnInit {

  public workout: Workout;
  public currentActivityIndex = 0;
  public exerciseActivitys: ExerciseActivity[] = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private workoutService: WorkoutService
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(
      param => this.getWorkout(param['id'])
    );
  }

  // getters and setters

  get currentActivity(): ExerciseActivity {
    return this.exerciseActivitys[this.currentActivityIndex];
  }

  get nextActivity(): ExerciseActivity {
    return this.exerciseActivitys[this.currentActivityIndex + 1];
  }

  get previousActivity(): ExerciseActivity {
    return this.exerciseActivitys[this.currentActivityIndex - 1];
  }

  // events

  public onStartStopWatch() {
    this.timer();
  }

  // pulbic

  public getActivityAtIndex(index: number): ExerciseActivity {
    return index >= 0 && index < this.exerciseActivitys.length
      ? this.exerciseActivitys[index]
      : null;
  }

  // private

  private getWorkout(workoutId: number) {
    this.workoutService.getWorkout(workoutId).subscribe(
      workout => {
        this.workout = workout;
        this.workout.workoutExercises.forEach(
          workoutExercise => this.exerciseActivitys = this.exerciseActivitys.concat(this.getSuperSetToTrack(workoutExercise))
        );
        this.exerciseActivitys = this.exerciseActivitys.filter(exerciseActivity => exerciseActivity);
      }
    );
  }

  private timer() {
    setTimeout(() => this.addASecond(), 1000);
  }

  private addASecond() {
    if (this.currentActivity.isTimeUp) {
      this.currentActivityIndex++;
    }
    this.currentActivity.time++;
    this.timer();
  }

  private getSuperSetToTrack(workoutExercise: WorkoutExercise): ExerciseActivity[] {
    const sets: ExerciseActivity[] = this.getSuperSetInitValue(workoutExercise);
    for (let exerciseIndex = 0; exerciseIndex < workoutExercise.exercises.length; exerciseIndex++) {
      const exercise = workoutExercise.exercises[exerciseIndex];
      for (let setIndex = 0; setIndex < exercise.sets.length; setIndex++) {
        const set = exercise.sets[setIndex];
        const index = ((setIndex * exercise.sets.length) + exerciseIndex) * 2;
        sets[index] = new ExerciseActivity(set, exercise.type);
        sets[index + 1] = new ExerciseActivity(set.restPeriod);
      }
    }
    return sets;
  }

  private getSuperSetInitValue(workoutExercise: WorkoutExercise): ExerciseActivity[] {
    const longestSetLength = Math.max(...workoutExercise.exercises.map(
      exercise => exercise.sets.length || 0
    ));
    return new Array(longestSetLength * workoutExercise.exercises.length * 2);
  }

}
