import { ExerciseSet } from '../../exercise/exercise-set/exercise-set.model';
import { ExerciseType } from '../../exercise/exercise-type/exercise-type.model';
import { Exercise } from '../../exercise/exercise/exercise.model';

export class ExerciseActivity {

    public time = 0;
    public estimatedTime: number;
    public forceEstimatedTime = false;
    public activityType: 'exercise' | 'rest';
    public exercise: Exercise;
    public set: ExerciseSet;
    public exerciseType: ExerciseType;
    public reps: number;
    public weight: number;

    constructor(rest: number);
    constructor(set: ExerciseSet, exerciseType: ExerciseType);
    constructor(restOrSet?: ExerciseSet | number, exerciseType?: ExerciseType) {
        if (restOrSet instanceof ExerciseSet) {
            this.initExercise(restOrSet, exerciseType);
        } else if (!isNaN(restOrSet)) {
            this.initRest(restOrSet);
        }
    }

    get isTimeUp(): boolean {
        return this.estimatedTime <= this.time;
    }

    private initExercise(set: ExerciseSet, type: ExerciseType) {
        this.estimatedTime = set.reps * type.repTime;
        this.exercise = set.exercise;
        this.exerciseType = type;
        this.reps = set.reps;
        this.weight = set.weight;
        this.activityType = 'exercise';
    }

    private initRest(time: number) {
        this.estimatedTime = time;
        this.forceEstimatedTime = true;
        this.exerciseType = new ExerciseType({ name: 'Rest' });
        this.activityType = 'rest';
    }

}
