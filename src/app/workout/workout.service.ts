import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Workout, IWorkout } from './workout.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { WorkoutDay, IWorkoutDay } from './workout-day/workout-day.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WorkoutService {

  constructor(
    private httpClient: HttpClient
  ) { }

  // getters and setters

  private get url(): string {
    return environment.apiUrl + '/workouts';
  }

  // public

  public getWorkout(id: number): Observable<Workout> {
    return this.httpClient.get<IWorkout>(`${this.url}/${id}`)
      .pipe(map(iWorkout => new Workout(iWorkout)));
  }

  public getWorkoutDays(from: string, to: string): Observable<WorkoutDay[]> {
    const params: HttpParams = new HttpParams()
      .set('from-date', from)
      .set('to-date', to);
    return this.httpClient.get<IWorkoutDay[]>(`${this.url}/days`, { params: params })
      .pipe(map(workoutDays => workoutDays.map(workoutDay => new WorkoutDay(workoutDay))));
  }

  public saveWorkout(workout: Workout): Observable<Workout> {
    return this.httpClient.post<IWorkout>(this.url, workout)
      .pipe(map(iWorkout => new Workout(iWorkout)));
  }

}
