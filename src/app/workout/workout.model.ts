import { User, IUser } from '../user/user.model';
import { IWorkoutExercise, WorkoutExercise } from '../exercise/workout-exercise/workout-exercise.model';
import { Program, IProgram } from '../program/program.model';
import { BaseModel, IBaseModel } from '../shared/model/base.model';

export interface IWorkout extends IBaseModel {
    name?: string;
    sequenceNumber?: number;
    user?: IUser;
    program?: IProgram;
    workoutDate?: string;
    workoutExercises?: IWorkoutExercise[];
}

export class Workout extends BaseModel {

    public name: string;
    public sequenceNumber: number;
    public user: User;
    public program: Program;
    public workoutDate: string;
    public workoutExercises: WorkoutExercise[];

    constructor(iWorkout: IWorkout) {
        if (iWorkout) {
            super(iWorkout);
            this.name = iWorkout.name;
            this.sequenceNumber = iWorkout.sequenceNumber;
            this.user = iWorkout.user ? new User(iWorkout.user) : null;
            this.program = iWorkout.program ? new Program(iWorkout.program) : null;
            this.workoutDate = iWorkout.workoutDate;
            this.workoutExercises = iWorkout.workoutExercises
                ? iWorkout.workoutExercises.map(exercise => new WorkoutExercise(exercise)) : [];
        }
    }

}
