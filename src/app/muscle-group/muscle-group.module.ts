import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MuscleGroupService } from './muscle-group.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    MuscleGroupService
  ]
})
export class MuscleGroupModule { }
