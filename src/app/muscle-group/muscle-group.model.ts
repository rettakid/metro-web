export interface IMuscleGroup {

    id?: number;
    name?: string;
    surname?: string;
}

export class MuscleGroup {

    public id: number;
    public name: string;
    public surname: string;

    constructor(iMuscleGroup: IMuscleGroup) {
        if (iMuscleGroup) {
            this.id = iMuscleGroup.id;
            this.name = iMuscleGroup.name;
            this.surname = iMuscleGroup.surname;
        }
    }

}
