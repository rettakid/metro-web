import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { MuscleGroup, IMuscleGroup } from './muscle-group.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MuscleGroupService {

  constructor(
    private httpCLient: HttpClient
  ) { }

  private get url(): string {
    return environment.apiUrl + '/muscle-groups';
  }

  public getMuscleGroups(): Observable<MuscleGroup[]> {
    return this.httpCLient.get<IMuscleGroup[]>(this.url)
      .pipe(map(iMuscleGroups => iMuscleGroups.map(iMuscleGroup => new MuscleGroup(iMuscleGroup))));
  }

}
