export interface IFileObject {
    id?: number;
    name?: string;
    mimeType?: string;
}

export class FileObject {

    public id: number;
    public name: string;
    public mimeType: string;

    constructor(iFileObject: IFileObject) {
        if (iFileObject) {
            this.id = iFileObject.id;
            this.name = iFileObject.name;
            this.mimeType = iFileObject.mimeType;
        }
    }

}
