import { IFileObject, FileObject } from './file-object.model';

export interface IImageFile {
    id?: number;
    file?: IFileObject;
}

export class ImageFile {

    public id: number;
    public file: FileObject;

    constructor(iImageFile: IImageFile) {
        if (iImageFile) {
            this.id = iImageFile.id;
            this.file = new FileObject(iImageFile.file);
        }
    }

}
