import { IFileObject, FileObject } from './file-object.model';

export interface IVideoFile {
    id?: number;
    file?: IFileObject;
    thumbnail?: IFileObject;
}

export class VideoFile {

    public id: number;
    public file: FileObject;
    public thumbnail: FileObject;

    constructor(iVideoFile: IVideoFile) {
        if (iVideoFile != null) {
            this.id = iVideoFile.id;
            this.file = new FileObject(iVideoFile.file);
            this.thumbnail = new FileObject(iVideoFile.thumbnail);
        }
    }

}
