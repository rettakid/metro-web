import { NgModule } from '@angular/core';
import { NavModule } from './nav/nav.module';
import { RouterModule } from '@angular/router';
import { ProgramModule } from '../program/program.module';
import { MuscleGroupModule } from '../muscle-group/muscle-group.module';
import { SideNavModule } from './side-nav/side-nav.module';
import { LoginComponent } from './login/login.component';
import { LoginModule } from './login/login.module';
import { WorkoutCalendarComponent } from '../workout/workout-calendar/workout-calendar.component';
import { WorkoutModule } from '../workout/workout.module';
import { WorkoutDayComponent } from '../workout/workout-day/workout-day.component';
import { WorkoutTrackerComponent } from '../workout/workout-tracker/workout-tracker.component';

@NgModule({
  imports: [
    RouterModule.forRoot([
      { path: 'login', component: LoginComponent },
      { path: 'workout/calendar', component: WorkoutCalendarComponent },
      { path: 'workout/calendar/:date', component: WorkoutDayComponent },
      { path: 'workout/:id/track', component: WorkoutTrackerComponent },
    ])
  ],
  exports: [
    LoginModule,
    MuscleGroupModule,
    NavModule,
    ProgramModule,
    RouterModule,
    SideNavModule,
    WorkoutModule
  ],
  declarations: []
})
export class CoreModule { }
