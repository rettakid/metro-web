import { Workout, IWorkout } from '../../workout/workout.model';
import { Exercise, IExercise } from '../exercise/exercise.model';
import { IBaseModel, BaseModel } from '../../shared/model/base.model';

export interface IWorkoutExercise {
    id?: number;
    validFrom?: string;
    validTo?: string;
    workout?: IWorkout;
    restPeriod?: number;
    exercises?: IExercise[];
}

export class WorkoutExercise {

    public id: number;
    public validFrom: string;
    public validTo: string;
    public workout: Workout;
    public restPeriod: number;
    public exercises: Exercise[];

    constructor(iWorkoutExercise: IWorkoutExercise) {
        if (iWorkoutExercise) {
            this.id = iWorkoutExercise.id;
            this.validFrom = iWorkoutExercise.validFrom;
            this.validTo = iWorkoutExercise.validTo;
            this.workout = iWorkoutExercise.workout ? new Workout(iWorkoutExercise.workout) : null;
            this.restPeriod = iWorkoutExercise.restPeriod;
            this.exercises = iWorkoutExercise.exercises ? iWorkoutExercise.exercises.map(exercise => new Exercise(exercise)) : [];
        }
    }

}

