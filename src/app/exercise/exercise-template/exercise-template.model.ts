import { Program } from '../../program/program.model';
import { IExerciseType, ExerciseType } from '../exercise-type/exercise-type.model';
import { MuscleGroup, IMuscleGroup } from '../../muscle-group/muscle-group.model';

export interface IExerciseTemplate {
    id?: number;
    program?: Program;
    exerciseType?: IExerciseType;
    muscleGroup?: IMuscleGroup;
    randomise?: boolean;
}

export class ExerciseTemplate {

    public id: number;
    public program: Program;
    public exerciseType: ExerciseType;
    public muscleGroup: MuscleGroup;
    public randomise: boolean;

    constructor(iExerciseTemplate: IExerciseTemplate) {
        if (iExerciseTemplate) {
            this.id = iExerciseTemplate.id;
            this.program = iExerciseTemplate.program ? new Program(iExerciseTemplate.program) : null;
            this.exerciseType = iExerciseTemplate.exerciseType ? new ExerciseType(iExerciseTemplate.exerciseType) : null;
            this.muscleGroup = iExerciseTemplate.muscleGroup ? new MuscleGroup(iExerciseTemplate.muscleGroup) : null;
            this.randomise = iExerciseTemplate.randomise;
        }
    }

}
