import { TestBed, inject } from '@angular/core/testing';

import { ExerciseTypeService } from './exercise-type.service';

describe('ExerciseTypeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExerciseTypeService]
    });
  });

  it('should be created', inject([ExerciseTypeService], (service: ExerciseTypeService) => {
    expect(service).toBeTruthy();
  }));
});
