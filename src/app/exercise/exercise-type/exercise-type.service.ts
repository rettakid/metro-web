import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { ExerciseType, IExerciseType } from '../exercise-type/exercise-type.model';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ExerciseTypeService {

  constructor(
    private httpClient: HttpClient
  ) { }

  get url() {
    return `${environment.apiUrl}/exercise-types`;
  }

  // public

  public getAllExerciseTypes(): Observable<ExerciseType[]> {
    return this.httpClient.get<IExerciseType[]>(this.url)
      .pipe(map(types => types.map(type => new ExerciseType(type))));
  }

}
