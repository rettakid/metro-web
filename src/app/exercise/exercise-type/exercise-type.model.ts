import { IImageFile, ImageFile } from '../../file/image-file.model';
import { IVideoFile, VideoFile } from '../../file/video-file.model';
import { IExerciseMuscleGroupImpacts, ExerciseMuscleGroupImpact } from '../exercise-muscle-group-impact/exercise-muscle-group-impact.model';

export interface IExerciseType {
    id?: number;
    name?: string;
    description?: string;
    bodyWeighImpact?: number;
    requiresEquipment?: boolean;
    requiresWeights?: boolean;
    isometric?: boolean;
    timeBased?: boolean;
    concentricPeriod?: number;
    concentricHoldPeriod?: number;
    eccentricPeriod?: number;
    eccentricHoldPeriod?: number;
    image?: IImageFile;
    video?: IVideoFile;
    muscleGroupImpacts?: IExerciseMuscleGroupImpacts[];
}

export class ExerciseType {

    public id: number;
    public name: string;
    public description: string;
    public bodyWeighImpact: number;
    public requiresEquipment: boolean;
    public requiresWeights: boolean;
    public isometric: boolean;
    public timeBased: boolean;
    public concentricPeriod?: number;
    public concentricHoldPeriod?: number;
    public eccentricPeriod?: number;
    public eccentricHoldPeriod?: number;
    public image: IImageFile;
    public video: IVideoFile;
    public muscleGroupImpacts: IExerciseMuscleGroupImpacts[];

    constructor(iExerciseType: IExerciseType) {
        if (iExerciseType) {
            this.id = iExerciseType.id;
            this.name = iExerciseType.name;
            this.description = iExerciseType.description;
            this.bodyWeighImpact = iExerciseType.bodyWeighImpact;
            this.requiresEquipment = iExerciseType.requiresEquipment;
            this.requiresWeights = iExerciseType.requiresWeights;
            this.isometric = iExerciseType.isometric;
            this.timeBased = iExerciseType.timeBased;
            this.concentricPeriod = iExerciseType.concentricPeriod;
            this.concentricHoldPeriod = iExerciseType.concentricHoldPeriod;
            this.eccentricPeriod = iExerciseType.eccentricPeriod;
            this.eccentricHoldPeriod = iExerciseType.eccentricHoldPeriod;
            this.image = iExerciseType.image ? new ImageFile(iExerciseType.image) : null;
            this.video = iExerciseType.video ? new VideoFile(iExerciseType.video) : null;
            this.muscleGroupImpacts = iExerciseType.muscleGroupImpacts
                ? iExerciseType.muscleGroupImpacts.map(impacts => new ExerciseMuscleGroupImpact(impacts)) : [];
        }
    }

    get repTime(): number {
        return this.concentricPeriod + this.concentricHoldPeriod + this.eccentricPeriod + this.eccentricHoldPeriod;
    }

}
