import { IExercise, Exercise } from '../exercise/exercise.model';
import { IBaseModel, BaseModel } from '../../shared/model/base.model';

export interface IExerciseSet extends IBaseModel {
    sequenceNumber?: number;
    weight?: number;
    reps?: number;
    restPeriod?: number;
    toFailure?: boolean;
    difficulty?: number;
    exercise?: IExercise;
}

export class ExerciseSet extends BaseModel {

    public sequenceNumber: number;
    public weight: number;
    public reps: number;
    public restPeriod: number;
    public toFailure: boolean;
    public difficulty: number;
    public exercise: Exercise;

    constructor(iExerciseSet: IExerciseSet) {
        if (iExerciseSet) {
            super(iExerciseSet);
            this.sequenceNumber = iExerciseSet.sequenceNumber;
            this.weight = iExerciseSet.weight;
            this.reps = iExerciseSet.reps;
            this.restPeriod = iExerciseSet.restPeriod;
            this.toFailure = iExerciseSet.toFailure;
            this.difficulty = iExerciseSet.difficulty;
            this.exercise = iExerciseSet.exercise ? new Exercise(iExerciseSet.exercise) : null;
        }
    }

}
