import { IExerciseSet, ExerciseSet } from '../exercise-set/exercise-set.model';
import { IExerciseTemplate, ExerciseTemplate } from '../exercise-template/exercise-template.model';
import { IExerciseType, ExerciseType } from '../exercise-type/exercise-type.model';
import { WorkoutExercise, IWorkoutExercise } from '../workout-exercise/workout-exercise.model';
import { BaseModel, IBaseModel } from '../../shared/model/base.model';

export interface IExercise extends IBaseModel {
    sequenceNumber?: number;
    workoutExercise?: IWorkoutExercise;
    exerciseTemplate?: IExerciseTemplate;
    restPeriod?: number;
    type?: IExerciseType;
    sets?: IExerciseSet[];
}

export class Exercise extends BaseModel {

    public sequenceNumber: number;
    public workoutExercise: WorkoutExercise;
    public exerciseTemplate: ExerciseTemplate;
    public restPeriod: number;
    public type: ExerciseType;
    public sets: ExerciseSet[];

    constructor(iExercise: IExercise) {
        if (iExercise) {
            super(iExercise);
            this.sequenceNumber = iExercise.sequenceNumber;
            this.workoutExercise = iExercise.workoutExercise ? new WorkoutExercise(iExercise.workoutExercise) : null;
            this.exerciseTemplate = iExercise.exerciseTemplate ? new ExerciseTemplate(iExercise.exerciseTemplate) : null;
            this.restPeriod = iExercise.restPeriod;
            this.type = iExercise.type ? new ExerciseType(iExercise.type) : null;
            this.sets = iExercise.sets ? iExercise.sets.map(set => new ExerciseSet(set)) : [];
        }
    }

}
