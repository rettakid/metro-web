import { IExerciseType, ExerciseType } from '../exercise-type/exercise-type.model';
import { IMuscleGroup, MuscleGroup } from '../../muscle-group/muscle-group.model';

export interface IExerciseMuscleGroupImpacts {
    id?: number;
    exerciseType?: IExerciseType;
    muscleGroup?: IMuscleGroup;
    symmetrical?: boolean;
    impact?: number;
}

export class ExerciseMuscleGroupImpact {

    public id: number;
    public exerciseType: ExerciseType;
    public muscleGroup: MuscleGroup;
    public symmetrical: boolean;
    public impact: number;

    constructor(iExerciseMuscleGroupImpacts: IExerciseMuscleGroupImpacts) {
        if (iExerciseMuscleGroupImpacts) {
            this.id = iExerciseMuscleGroupImpacts.id;
            this.exerciseType = iExerciseMuscleGroupImpacts.exerciseType
                ? new ExerciseType(iExerciseMuscleGroupImpacts.exerciseType) : null;
            this.muscleGroup = iExerciseMuscleGroupImpacts.muscleGroup
                ? new MuscleGroup(iExerciseMuscleGroupImpacts.muscleGroup) : null;
            this.symmetrical = iExerciseMuscleGroupImpacts.symmetrical;
            this.impact = iExerciseMuscleGroupImpacts.impact;
        }
    }

}
