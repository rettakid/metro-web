import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResizableGridColComponent } from './resizable-grid-col/resizable-grid-col.component';
import { ResizableGridService } from '../resizable-grid.service';

@NgModule({
  exports: [
    ResizableGridColComponent
  ],
  declarations: [ResizableGridColComponent],
  providers: [ResizableGridService]
})
export class ResizableGridModule { }
