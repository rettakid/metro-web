import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResizableGridColComponent } from './resizable-grid-col.component';

describe('ResizableGridColComponent', () => {
  let component: ResizableGridColComponent;
  let fixture: ComponentFixture<ResizableGridColComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResizableGridColComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResizableGridColComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
