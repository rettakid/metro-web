import { ResizableGridModule } from './resizable-grid.module';

describe('ResizableGridModule', () => {
  let resizableGridModule: ResizableGridModule;

  beforeEach(() => {
    resizableGridModule = new ResizableGridModule();
  });

  it('should create an instance', () => {
    expect(resizableGridModule).toBeTruthy();
  });
});
