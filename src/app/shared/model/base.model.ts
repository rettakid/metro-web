export interface IBaseModel {
    id?: number;
    validFrom?: string;
    validTo?: string;
}

export class BaseModel {

    public id: number;
    public validFrom: string;
    public validTo: string;

    constructor(iBaseModel: IBaseModel) {
        if (iBaseModel) {
            this.id = iBaseModel.id;
            this.validFrom = iBaseModel.validFrom;
            this.validTo = iBaseModel.validTo;
        }
    }

}
