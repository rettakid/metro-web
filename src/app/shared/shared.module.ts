import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ModalModule } from './modal/modal.module';
import { RouterModule } from '@angular/router';
import { StorageModule } from './storage/storage.module';

@NgModule({
  exports: [
    CommonModule,
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ModalModule,
    StorageModule,
    RouterModule
  ],
  declarations: []
})
export class SharedModule { }
