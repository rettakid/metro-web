export enum StorageEnum {
    USER = <any>'user',
    SESSION_TOKEN = <any>'session-token',
    ERRORS = <any>'errors',
    PROJECT_TO_CREATE = <any>'project-to-create'
}