import { Injectable } from '@angular/core';
import { Subscriber, Observable } from 'rxjs';
import { StorageEnum } from './storage.enum';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  private storageMap: { [key: string]: Storage } = {};

  private subscriber: Subscriber<any>;
  private subscribedTo: StorageEnum;

  constructor() {
    this.storageMap[StorageEnum.USER] = sessionStorage;
    this.storageMap[StorageEnum.SESSION_TOKEN] = sessionStorage;
    this.storageMap[StorageEnum.ERRORS] = sessionStorage;
    this.storageMap[StorageEnum.PROJECT_TO_CREATE] = sessionStorage;
  }

  public watch(stoageEnum: StorageEnum): Observable<any> {
    this.subscribedTo = stoageEnum;
    const observable: Observable<any> = new Observable(
      subscriber => this.subscriber = subscriber
    );
    return observable;
  }

  public get(storageEnum: StorageEnum): any {
    const result = this.storageMap[storageEnum].getItem(storageEnum.toString());
    return JSON.parse(result);
  }

  public set(storageEnum: StorageEnum, data: any) {
    const result = JSON.stringify(data);
    this.storageMap[storageEnum].setItem(storageEnum.toString(), result);
    this.pushToSubsciber(storageEnum, data);
  }

  public remove(storageEnum: StorageEnum) {
    this.pushToSubsciber(storageEnum, null);
    return this.storageMap[storageEnum].removeItem(storageEnum.toString());
  }

  public removeAllSession() {
    this.pushToSubsciber(null, null);
    sessionStorage.clear();
  }

  public destroy() {
    this.subscriber.complete();
  }

  // private

  private pushToSubsciber(storageEnum: StorageEnum, data: any) {
    if (this.subscribedTo === (storageEnum || this.subscribedTo)) {
      this.subscriber.next(data);
    }
  }

}
