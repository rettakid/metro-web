import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  @Input() modalId: string;
  @Input() title: string;
  @Input() secondaryButtonText: string;
  @Output() submit: EventEmitter<any> = new EventEmitter<any>();
  @Output() secondarySubmit: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  // events

  public onSubmitEvent() {
    this.submit.emit();
  }

  public onSecondarySubmitEvent() {
    this.secondarySubmit.emit();
  }

}
