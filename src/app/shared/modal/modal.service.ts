import { Injectable } from '@angular/core';

declare var $;

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor() { }

  public toggle(id: string) {
    $(`#${id}`).modal('toggle');
  }

  public show(id: string) {
    $(`#${id}`).modal('show');
  }

  public hide(id: string) {
    $(`#${id}`).modal('hide');
  }

}
