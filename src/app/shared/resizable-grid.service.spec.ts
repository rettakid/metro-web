import { TestBed, inject } from '@angular/core/testing';

import { ResizableGridService } from './resizable-grid.service';

describe('ResizableGridService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ResizableGridService]
    });
  });

  it('should be created', inject([ResizableGridService], (service: ResizableGridService) => {
    expect(service).toBeTruthy();
  }));
});
